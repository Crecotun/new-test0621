import $ from 'jquery'
import 'vendor/jquery.validate'

$(document).ready(() => {

	$('.modal-block__close').on('click', function (e) {
		e.preventDefault();
		$('.modal-block').removeClass('show');
		$('body').removeClass('overflow');
	});
	$('.js-modal').on('click', function (e) {
		$('.modal-block').addClass('show');
		$('body').addClass('overflow');
		e.preventDefault();
	});

	//фокус полей
	$('input,textarea').focus(function () {
		$(this).data('placeholder', $(this).attr('placeholder'))
		$(this).attr('placeholder', '')
	})
	$('input,textarea').blur(function () {
		$(this).attr('placeholder', $(this).data('placeholder'))
	})

	$(function(){
		$('.form').validate({
			success: 'valid',
			submitHandler: function() {
				$('.modal-block').removeClass('show');
				$('body').removeClass('overflow');
			},
			rules: {
				field: {
					required: true,
				},
			},
			messages: {
				mail: {
					required: 'Укажите Ваш mail'
				},
				pass: {
					required: 'Введите пароль'
				},
			}
		});
	});



	

})